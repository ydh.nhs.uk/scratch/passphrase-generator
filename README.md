## Background

This is some sample code to generate random passphrases (as opposed to passwords consisting of random characters).
There are two reasons for doing this:

- It improves password entropy, thus creating passwords that are harder to crack (see [https://xkcd.com/936/](https://xkcd.com/936/))
- A previous project that I wrote generated random passwords that were then sent to the respective users.
  The users had trouble entering these (and were seemingly unable to copy & paste...), for example due to
  mistaking a 1 for a lowercase L, etc. A passphrase will be easier to read and less error-prone when 
  entered manually.

## Description of code

This is only intended to be rough sample code so has been written as a console app, but it would be trivial
to turn this into a simple Web API.

It uses 3 dictionary files, contained in the data directory - the more words, the better.

It then uses a cryptographically secure algorithm to choose words at random from the list - the number
of words and word separator are defined in static variables.
