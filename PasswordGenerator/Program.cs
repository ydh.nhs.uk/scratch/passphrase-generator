﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace PasswordGenerator
{
    public static class Program
    {
        private static readonly int NUM_WORDS = 4;
        private static readonly char SEPARATOR = '-';

        public static void Main(string[] args)
        {
            string[] words = CreateDictionary();
            string password = GeneratePassword(words);

            Console.WriteLine($"Password is: {password}");
            Console.ReadLine();
        }

        private static string[] CreateDictionary()
        {
            List<string> allWords = new List<string>();
            string[] files = { "data/jargon.txt", "data/science-terms.txt", "data/wordlist.txt" };

            foreach (var filename in files)
            {
                var allText = File.ReadAllText(filename);
                var words = allText.Split(",");
                allWords.AddRange(words);
            }

            Console.WriteLine($"Found {allWords.Count} words");
            return allWords.ToArray();
        }

        private static string GeneratePassword(string[] words)
        {
            StringBuilder sb = new StringBuilder();

            using RNGCryptoServiceProvider rngCsp = new RNGCryptoServiceProvider();
            for (int i = 1; i <= NUM_WORDS; i++)
            {
                string word = GenerateRandomWord(words, rngCsp);
                sb.Append(word);
                if (i != NUM_WORDS)
                {
                    sb.Append(SEPARATOR);
                }
            }

            return sb.ToString();
        }

        private static string GenerateRandomWord(string[] words, RNGCryptoServiceProvider rngCsp)
        {
            // Create a 4-byte array, which will convert into an int
            var byteArray = new byte[4];
            rngCsp.GetBytes(byteArray);

            UInt32 randomInteger;
            do
            {
                randomInteger = BitConverter.ToUInt32(byteArray, 0);
            }
            while (!IsValidInteger(randomInteger, words.Length));

            Console.WriteLine($"Generated random integer: {randomInteger} -> word {randomInteger % words.Length}");
            // Return the random number mod the number of words
            return words[randomInteger % words.Length];
        }

        private static bool IsValidInteger(UInt32 randomInteger, int numWords)
        {
            // There are Int32.MaxValue / numWords full set of numbers that can come up. 
            // The final set will be incomplete and we should not choose a number from this set
            var fullSetsOfValues = UInt32.MaxValue / numWords;

            // If the random number is within the range of full sets, return true
            return randomInteger < (numWords * fullSetsOfValues);
        }
    }
}
